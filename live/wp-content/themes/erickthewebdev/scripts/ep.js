jQuery(document).ready(
    function($){
        $("body").attr("id","ep-body");
        $(".et_pb_post").append("<div class='ep-blog-content__wrapper'></div>");
        $(".entry-title").appendTo(".ep-blog-content__wrapper");
        $(".post-meta").appendTo(".ep-blog-content__wrapper");
        $(".post-content").appendTo(".ep-blog-content__wrapper");
        AOS.init(
            {
                disable: "mobile"
            }
        );
    }
);